from cv2 import cvtColor, inRange, findContours, drawContours, COLOR_BGR2HSV

from cv_bridge import CvBridge
from sensor_msgs.msg import Image

import rclpy
from rclpy.node import Node

class maskWhiteBoxes(Node):

    def __init__(self):
        super().__init__('maskWhiteBoxes')
        self.publisher_ = self.create_publisher(Image, 'camera/box_seg', 10)
        self.subscription_ = self.create_subscription(Image, 'camera/image_raw', self.listener_callback, 10)
        self.bridge = CvBridge()
        
    def listener_callback(self, msg):
        # Note that conversion call passes through (rgb8 -> rgb8)
        imgHSV = cv2.cvtColor( self.bridge.imgmsg_to_cv2(msg, desired_encoding='passthrough'),
            cv2.COLOR_RGB2HSV)
        imgMasked = cv2.inRange(imgHSV, (0,0,200),(180,20,255))
        contours, contourHier = cv2.findContours(imgMasked, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE)
        # Perform the drawing of contours and pass the image through the conversion
        outmsg = self.bridge.cv2_to_imgmsg(cv2.drawContours(imgMasked, contours, -1, (255), 1), 'passthrough')
        self.publisher_.publish(outmsg)

def main(args=None):
    rclpy.init(args=args)

    maskwhiteboxes = maskWhiteBoxes()

    rclpy.spin(maskwhiteboxes)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    maskwhiteboxes.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()

